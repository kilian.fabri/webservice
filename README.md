1. Clone the repository
2. Change Branch to version_1 ('git checkout version_1')
3. Open the project with Intellij
4. Navigate to 'File' in the upper right corner, then select 'Project Structure' and after this 'Libraries'
5. In there you need to add all Libraries, just click the Plus Icon and select Java
6. Navigate to the 'lib' folder and add all 8 .jar files to the project
7. After you added all .jar files click the OK Button

____________________________________________________________________________________________________________________________________________________

8. To get data you need a database, we use Postgres, you can downloade the latest version at https://www.postgresql.org/
9. Execute the installer and install pgAdmin4
10. Open pgAdmin4 and log in
11. Navigate to Servers->PostgreSQL 14->Login/Group Roles and right click on it
12. Select Create, then Login/Group Role...
13. Under General type 'tim' into 'Name'
14. Under Definition type '12345' into 'Password'
15. Under Privileges turn 'Can log in?' on
16. Click the Save Button
17. Navigate to Servers->PostgreSQL 14->Database and right click on it
18. Select Create, then Database...
19. Under General type 'webserver' into 'Database'
20. Under Security click the plus at Privileges
21. Select an item-> tim, Privileges-> all
22. Click the Save Button
23. Navigate to Servers->PostgreSQL 14->Database->Schemas->public-Tables and right click on it
24. Select Query Tool copy into the Query Editor:

    CREATE TABLE IF NOT EXISTS public.ws_user
    (
    id serial, name character varying(40) COLLATE pg_catalog."default" NOT NULL, email character varying(40) COLLATE
    pg_catalog."default" NOT NULL, username character varying(40) COLLATE pg_catalog."default" NOT NULL, password
    character varying(40) COLLATE pg_catalog."default" NOT NULL, CONSTRAINT ws_users_pkey PRIMARY KEY (id), CONSTRAINT
    email UNIQUE (email), CONSTRAINT username UNIQUE (username)
    )

    TABLESPACE pg_default;

    ALTER TABLE IF EXISTS public.ws_user OWNER to tim;

25. Click the Execute/Refresh Button or press F5

____________________________________________________________________________________________________________________________________________________

24. Open Intellij again
25. Select Database in the upper right corner of the screen
26. Click on the plus, Data Source, PostgreSQL
27. Name: what ever you want, Host: localhost, port: 5432,
28. Authentication: User & Password, User: tim, Password: 12345
29. Database: webserver, URL: jdbc:postgresql://localhost:5432/webserver
30. Click the OK Button

____________________________________________________________________________________________________________________________________________________

31. Now you can Run the Main of the webservice in Intellij

____________________________________________________________________________________________________________________________________________________

32. For sending HTTP Requests you can use Postman, you can downloade it at https://www.postman.com/downloads/
33. Execute the installer and install Postman
34. Open Postman, select new, then HTTP Request
35. For every new Request you need to:
    Go to Authorization, select Basic Auth as Type, Username: admin, Password: password123!

-------------GET-------------------------------------------------------------------------------------

36. To get all Users from the Database the GET Url is:
    http://localhost:1080/user
37. To one User by ID from the Database the GET Url is:
    http://localhost:1080/user/{userId}
38. To one User by email from the Database the GET Url is:
    http://localhost:1080/user?email={email}
38. To more than one User by email from the Database the GET Url is:
    http://localhost:1080/user?email={email}&email={email}
    you can add as much &email=... as you like

-------------POST--------------------------------------------------------------------------------

39. To add a new User to the Database the POST Url is:
    http://localhost:1080/user
    The Data is written in the body with a Gson Format and looks like this:

    {
    "name": "Tim",
    "email": "tim@email.de",
    "username": "tim123",
    "password": "1234"
    }

    Attention: name and email have to be UNIQUE, there can't be 2 Users with the same values

-------------PUT---------------------------------------------------------------------------------------

40. To alter/update a User by ID in the Database the PUT Url is:
    http://localhost:1080/user/{userId}
    The Data is written in the body with a Gson Format and looks like this:

    {
    "name": "Tim",
    "email": "tim@email.de",
    "username": "tim123",
    "password": "1234"
    }

    Attention: name and email have to be UNIQUE, there can't be 2 Users with the same values

-------------DELETE---------------------------------------------------------------------------------------

41. To delete a User by ID in the Database the DELETE Url is:
    http://localhost:1080/user/{userId}
