package com.qs.webserver.repository;

import com.qs.webserver.Main;
import com.qs.webserver.entity.User;
import com.qs.webserver.framework.context.ApplicationContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static org.junit.Assert.*;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//You need to change the SQL Statements in UserRepository to the Test_SQL_STATEMENTS in order for this to work
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
@RunWith(MockitoJUnitRunner.class)
public class UserRepositoryTest {

    private UserRepository userRepository;

    @Before
    public void setUp() throws SQLException, IOException {
        ApplicationContext applicationContext = new ApplicationContext(Main.class.getPackageName());
        userRepository = applicationContext.getBean(UserRepository.class.getCanonicalName(), UserRepository.class, true);
        userRepository.createTable();
    }

    private User createTestUser() {
        User user = new User();
        user.setName("Test1");
        user.setUsername("Test1");
        user.setEmail("test1234@test.com");
        user.setPassword("123456");
        return user;
    }

    private User createTestUser2(Long userId) {
        User user = new User();
        user.setName("Test2");
        user.setUsername("Test2");
        user.setEmail("test21234@test.com");
        user.setPassword("1234562");
        user.setId(userId);
        return user;
    }

    @Test
    public void testCreateUser_Successful() {
        //Given
        User testUser = createTestUser();
        //Method under Test
        User userFromDB = userRepository.createUser(testUser);
        //Then
        assertNotNull(userFromDB.getId());
    }

    @Test
    public void testCreateUser_Failed() {
        //Given
        User testUser = createTestUser();
        testUser.setUsername(null);
        //Method under Test
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userRepository.createUser(testUser));
        //Then
        assertEquals("Wrong Exception message found", "Wrong Exception message found", actualException.getMessage());
    }

    @Test
    public void testFindAll_Successful() {
        //Given
        User testUser = createTestUser();
        User userFromDB = userRepository.createUser(testUser);
        User testUser2 = createTestUser2(3L);
        User userFromDB2 = userRepository.createUser(testUser2);
        //Method under Test
        Set<User> allUsers = userRepository.findAllUsers();
        //Then
        System.out.println(allUsers);
    }

    @Test
    public void testFindUserByID_Successful() {
        //Given
        User testUser = createTestUser();
        User userFromDB = userRepository.createUser(testUser);
        //Method under Test
        User userActual = userRepository.findUserById(userFromDB.getId());
        //Then
        assertEquals(testUser.getUsername(), userActual.getUsername());
    }


    @Test
    public void testFindUserByID_Failed() {
        //Given
        User testUser = createTestUser();
        userRepository.createUser(testUser);
        //Method under Test
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userRepository.findUserById(null));
        //Then
        assertNull("A user was found", actualException.getMessage());
    }

    @Test
    public void testFindUserByEmail_Successful() {
        //Given
        User testUser = createTestUser();
        User userFromDB = userRepository.createUser(testUser);
        //Method under Test
        User userActual = userRepository.findUserByEmail(userFromDB.getEmail());
        //Then
        assertEquals(testUser.getUsername(), userActual.getUsername());
    }

    @Test
    public void testFindUserByEmail_Failed() {
        //Given
        User testUser = createTestUser();
        userRepository.createUser(testUser);
        //Method under Test
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userRepository.findUserByEmail(null));
        //Then
        assertNull("A user was found", actualException.getMessage());
    }

    @Test
    public void testUpdateUser_Successful() {
        //Given
        User testUser = createTestUser();
        User userFromDB = userRepository.createUser(testUser);
        User testUpdateUser = createTestUser2(userFromDB.getId());
        //Method under Test
        User userActual = userRepository.updateUser(testUpdateUser);
        //Then
        assertEquals(testUpdateUser.getUsername(), userActual.getUsername());
    }

    @Test
    public void testUpdateUser_Failed() {
        //Method under Test
        User testUserDelete = userRepository.createUser(createTestUser());
        testUserDelete.setUsername(null);
        //Method under Test
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userRepository.updateUser(testUserDelete));
        //Then
        assertEquals("Wrong Exception message found", actualException.getMessage(), "Exception while updating user");
    }

    @Test
    public void deleteUser_Successful() {
        //Given
        User testUserDelete = userRepository.createUser(createTestUser());
        //Method under Test
        userRepository.deleteUser(testUserDelete);
        //Then
        User userById = userRepository.findUserById(testUserDelete.getId());
        assertNull(userById.getId());
    }

    @Test
    public void deleteUser_Failed() {
        //Given
        User testUserDelete = userRepository.createUser(createTestUser());
        testUserDelete.setId(null);
        //Method under Test
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userRepository.deleteUser(testUserDelete));
        //Then
        assertNull("User is not deleted", actualException.getMessage());
    }
}
