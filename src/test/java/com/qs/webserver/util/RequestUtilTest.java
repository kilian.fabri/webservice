package com.qs.webserver.util;

import com.qs.webserver.entity.User;
import com.qs.webserver.framework.util.RequestUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
class RequestUtilTest {

    @Mock
    public RequestUtil requestUtil;

    private User createTestUser(Long userId) {
        User user = new User();
        user.setName("Test");
        user.setUsername("Test1");
        user.setEmail("test1234@test.com");
        user.setPassword("test123456");
        user.setId(userId);
        return user;
    }


    @Test
    void creatResponse() {
        //Give
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String message = "message";
        String mimeType = "mimeType";
        int statusCode = 505;
        String status = "status";
        //Method under Test
        RequestUtil.creatResponse(byteArrayOutputStream, statusCode, status, mimeType, message);
        //Then
        String output = byteArrayOutputStream.toString();
        System.out.println(output);
    }

    @Test
    void testGetRequestBody_Successful() {
        //Given
        User testUser = createTestUser(1L);
        String userJson = RequestUtil.toJson(testUser, testUser.getClass());
        InputStream inputStream = new ByteArrayInputStream(userJson.getBytes(StandardCharsets.UTF_8));
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        int contentLength = userJson.getBytes(StandardCharsets.UTF_8).length;
        //Method under Test
        String body = RequestUtil.getRequestBody(reader, contentLength);
        //Then
        assertEquals(userJson, body);
    }
}