package com.qs.webserver.context;

import com.qs.webserver.dummyClasses.DummyClass;
import com.qs.webserver.framework.context.ApplicationContext;
import com.qs.webserver.framework.service.ConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationContextTest {
  ApplicationContext applicationContext = new ApplicationContext("");


  @Test
  public void testRegisterFactory_Successful() {
    //Given
    ConnectionFactory connectionFactory = new ConnectionFactory();
    //Method under Test
    applicationContext.registerFactory(connectionFactory.getClass());
    //Then
    Object bean = applicationContext.getBean(ConnectionFactory.class.getCanonicalName(), ConnectionFactory.class, true);
    assertEquals("Wrong class", connectionFactory.getClass(), bean.getClass());
  }

  @Test
  public void testRegisterFactory_CouldNotRegisterFactory() {
    //Given
    class DummyClass {
      private DummyClass() {
      }
    }
    DummyClass dummyClass = new DummyClass();
    //Method under Test Throws IllegalArgumentException
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.registerFactory(dummyClass.getClass()));
    //Then
    assertEquals("Wrong Exception Message", actualException.getMessage(), "The given FactoryClass doesn't contain any produces " + null);
  }

  @Test
  public void testRegisterBean_Successful() {
    //Given
    DummyClass dummyClass = new DummyClass();
    //Method under Test
    applicationContext.registerBean(dummyClass.getClass());
    //Then
    Object actualObject = applicationContext.getBean(DummyClass.class.getCanonicalName(), DummyClass.class, true);
    assertEquals("Wrong bean found", dummyClass.getClass(), actualObject.getClass());
  }

  @Test
  public void testRegisterBean_CouldNotRegisterBean() {
    //Given
    class DummyClass {
      private DummyClass() {
      }
    }
    DummyClass dummyClass = new DummyClass();
    //Method under Test
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.registerBean(dummyClass.getClass()));
    //Then
    assertEquals("Wrong Exception Message", actualException.getMessage(), "Could not register Bean " + DummyClass.class.getSimpleName());
  }

  @Test
  public void testRegisterBean_isEmpty() {
    //Method under Test
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.registerBean(ProcessHandle.class));
    //Then
    assertEquals("Wrong Exception Message", actualException.getMessage(), "Could not register Bean " + ProcessHandle.class.getSimpleName());
  }

  @Test
  public void testRegisterBean_Null() {
    //Method under Test
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.registerBean(applicationContext.getClass()));
    //Then
    assertEquals("Wrong Exception Message", actualException.getMessage(), "Bean " + applicationContext.getClass().getSimpleName() + " already exists");
  }


  @Test
  public void testRegisterFactory_isEmpty() {
    //Method under Test Throws RuntimeException
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.registerFactory(ProcessHandle.class));
    //Then
    assertEquals("Wrong Exception Message", actualException.getMessage(), "The given FactoryClass doesn't contain any produces " + ProcessHandle.class.getCanonicalName());
  }


  @Test
  public void testUnregisterBean_Successful() {
    //Given
    DummyClass dummyClass = new DummyClass();
    applicationContext.registerBean(dummyClass.getClass());
    //Method under Test
    applicationContext.unregisterBean(DummyClass.class.getCanonicalName());
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.getBean(DummyClass.class.getCanonicalName(), DummyClass.class, true));
    //Then
    assertEquals("Wrong Exception Message", "Could not find bean " + DummyClass.class.getCanonicalName(), actualException.getMessage());
  }

  @Test
  public void injectProperties() {
  }

}