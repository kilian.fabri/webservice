package com.qs.webserver.context;

import com.qs.webserver.framework.context.ApplicationClassLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;
import java.util.logging.Logger;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationClassLoaderTest {

  @Test
  public void findAllClasses_Successful() {
    //Given
    String packageName = Logger.class.getPackageName();
    ApplicationClassLoader applicationClassLoader = new ApplicationClassLoader(packageName);
    //Method under Test
    Map<String, Class<?>> allClasses = applicationClassLoader.findAllClasses();
    //Then
    assertTrue(allClasses.containsValue(Logger.class));
  }

  @Test
  public void testFindAllClasses_notFound() {
    //When
    String packageName = Logger.class.getPackageName();
    ApplicationClassLoader applicationClassLoader = new ApplicationClassLoader(packageName);
    when(applicationClassLoader.findAllClasses()).thenThrow(new RuntimeException("Class not found = "));
    //Method under Test throws RuntimeException
    RuntimeException actualException = assertThrows(RuntimeException.class, applicationClassLoader::findAllClasses);
    //Then
    assertEquals("Wrong exception Message", actualException.getMessage(), "Class not found = ");
  }
}