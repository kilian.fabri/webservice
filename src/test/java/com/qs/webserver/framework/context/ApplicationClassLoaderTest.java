package com.qs.webserver.framework.context;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class ApplicationClassLoaderTest {

  @Test
  public void testResolveCanonicalClassNameFromFile_Linux() {
    //Given
    System.setProperty("os.name", "Linux");
    String fileName = "/home/afeldmann/workspace/webservice-kf/./out/production/workspace/com/qs/webserver/dto/UserDto.class";
    String expectedClass = "com.qs.webserver.dto.UserDto";
    ApplicationClassLoader classLoader = new ApplicationClassLoader("com.qs.webserver");
    //Method under testing
    String actual = classLoader.getCanonicalClassName(fileName);
    //Then
    assertEquals("The canonical name was not resolved correctly.", expectedClass, actual);
  }

  @Test
  public void testResolveCanonicalClassNameFromFile_Windows() {
    //Given
    System.setProperty("os.name", "Windows");
    String fileName = "C:\\Users\\kili2\\workspace\\webservice\\.\\out\\production\\workspace\\com\\qs\\webserver\\dto\\UserDto.class";
    String expectedClass = "com.qs.webserver.dto.UserDto";
    ApplicationClassLoader classLoader = new ApplicationClassLoader("com.qs.webserver");
    //Method under testing
    String actual = classLoader.getCanonicalClassName(fileName);
    //Then
    assertEquals("The canonical name was not resolved correctly.", expectedClass, actual);
  }
}
