package com.qs.webserver.framework.jdbc;

import com.qs.webserver.Main;
import com.qs.webserver.dummyClasses.DummyUserRepo2;
import com.qs.webserver.entity.User;
import com.qs.webserver.framework.context.ApplicationContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Type;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class GenerateSQLStatementsTest {
    private GenerateSQLStatements generateSQLStatements;

    @Before
    public void setUp() {
        ApplicationContext applicationContext = new ApplicationContext(Main.class.getPackageName());
        generateSQLStatements = applicationContext.getBean(GenerateSQLStatements.class.getCanonicalName(), GenerateSQLStatements.class, true);
        //generateSQLStatements.generateAllSQLStatements();
    }

    @Test
    public void testGetTypeArgumentsFromRepositoryClass_Successful() {
        //Given
        Type[] testInterfaceTypes = {User.class, Long.class};
        //Method under testing
        Type[] actualInterfaceTypes = generateSQLStatements.getTypeArgumentsFromRepositoryClass(DummyUserRepo2.class);
        //Then
        assertEquals("Wrong interfaceTypes", testInterfaceTypes, actualInterfaceTypes);
    }

    @Test
    public void testRegisterTableSqlStatement_Successful() {
        //Given
        String testCreateTableSQL = "CREATE TABLE IF NOT EXISTS public.ws_user\n" +
                "(\n" +
                "id SERIAL PRIMARY KEY NOT NULL, \n" +
                "name VARCHAR NOT NULL, \n" +
                "username VARCHAR NOT NULL UNIQUE, \n" +
                "email VARCHAR NOT NULL UNIQUE, \n" +
                "password VARCHAR NOT NULL\n" +
                ")\n" +
                "TABLESPACE pg_default;";

        //Method under testing
        String actualCreateTableStatement = generateSQLStatements.getCreateTableStatement();
        //Then
        assertEquals("Wrong Statement", testCreateTableSQL, actualCreateTableStatement);
    }

    @Test
    public void testGenerateSaveEntitySQLStatement_Successful() {
        //Given
        String testFindUserByEmailSql = "INSERT INTO ws_user (name, username, email, password) VALUES (?, ?, ?, ?);";
        //Method under testing
        String actualFindUserByEmailStatement = generateSQLStatements.getSaveUserSQlStatement();
        //Then
        assertEquals("Wrong Statement", testFindUserByEmailSql, actualFindUserByEmailStatement);
    }

    @Test
    public void testGenerateFindByIdStatement_Successful() {
        //Given
        String testFindUserByEmailSql = "SELECT * FROM ws_user WHERE id = ?";
        //Method under testing
        String actualFindUserByIdStatement = generateSQLStatements.getFindByIdSQLStatement();
        //Then
        assertEquals("Wrong Statement", testFindUserByEmailSql, actualFindUserByIdStatement);
    }

    @Test
    public void testGenerateFindByEmailStatement_Successful() {
        //Given
        String testFindUserByEmailSql = "SELECT * FROM ws_user WHERE email = ?";
        //Method under testing
        String actualFindUserByEmailStatement = generateSQLStatements.getFindByEmailSQLStatement();
        //Then
        assertEquals("Wrong Statement", testFindUserByEmailSql, actualFindUserByEmailStatement);
    }

    @Test
    public void testGenerateDeleteByIdStatement() {
        //Given
        String testDeleteByIdSql = "DELETE FROM ws_user WHERE id = ?";
        //Method under testing
        String actualDeleteByIdStatement = generateSQLStatements.getDeleteByIdSQLStatement();
        //Then
        assertEquals("Wrong Statement", testDeleteByIdSql, actualDeleteByIdStatement);
    }

    @Test
    public void testFindAllStatement() {
        //Given
        String testFindAllSql = "SELECT * FROM ws_user";
        //Method under testing
        String actualFindAllStatement = generateSQLStatements.getFindAllSQLStatement();
        //Then
        assertEquals("Wrong Statement", testFindAllSql, actualFindAllStatement);
    }

    @Test
    public void testCountStatement() {
        //Given
        String testCountSql = "SELECT COUNT * FROM ws_user";
        //Method under testing
        String actualCountStatement = generateSQLStatements.getCountStatement();
        //Then
        assertEquals("Wrong Statement", testCountSql, actualCountStatement);
    }


}
