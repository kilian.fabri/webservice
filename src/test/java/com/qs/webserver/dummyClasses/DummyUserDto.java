package com.qs.webserver.dummyClasses;

import com.qs.webserver.entity.User;
import com.qs.webserver.framework.annotation.Column;
import com.qs.webserver.framework.annotation.Constraint;
import com.qs.webserver.framework.annotation.Id;
import com.qs.webserver.framework.annotation.Table;

@Table(table_name = "User")
public class DummyUserDto {
    @Id
    @Column(column_name = "id")
    private Long id;

    @Column(column_name = "name")
    private String name;

    @Constraint(constraint_name = "unique")
    @Column(column_name = "email")
    private String email;

    @Constraint(constraint_name = "unique")
    @Column(column_name = "username")
    private String username;

    @Column(column_name = "password")
    private String password;

    public DummyUserDto(long id, String name, String username, String email, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public DummyUserDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.password = user.getPassword();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
