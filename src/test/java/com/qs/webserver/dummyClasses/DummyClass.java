package com.qs.webserver.dummyClasses;

import com.qs.webserver.framework.annotation.Service;

@Service
public class DummyClass {
  public DummyClass() {
  }

  public void dummyMethod() {
  }
}
