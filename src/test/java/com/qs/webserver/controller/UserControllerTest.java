package com.qs.webserver.controller;


import com.qs.webserver.dto.UserDto;
import com.qs.webserver.entity.User;
import com.qs.webserver.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @Mock
    public UserService userService;

    @InjectMocks
    public UserController userController;

    public UserDto userDtoTest = new UserDto(createTestUser(1L));

    private User createTestUser(Long userId) {
        User user = new User();
        user.setName("Test");
        user.setUsername("Test");
        user.setEmail("test1234@test.com");
        user.setPassword("123456");
        user.setId(userId);
        return user;
    }

    @Test
    public void testSaveUser_Successful() {
        when(userService.saveUser(any())).thenReturn(createTestUser(1L));
        UserDto user = userController.saveUser(userDtoTest);
        //Then
        assertEquals(Long.valueOf(1L), user.getId());
    }

    @Test
    public void testSaveUser_AlreadyExists() {
        when(userService.saveUser(any())).thenThrow(new RuntimeException("Exception while creating user"));
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userController.saveUser(userDtoTest));
        //Then
        assertEquals("Wrong Exception message found", "Exception while creating user", actualException.getMessage());
    }

    @Test
    public void testGetAllUsers_Successful() {
        when(userService.saveUser(any())).thenReturn(createTestUser(1L));
        UserDto user = userController.saveUser(userDtoTest);
        //Method under Testing
        //Set<User> allUsers = userController.getUsers("");
        // System.out.println(allUsers);
    }

    @Test
    public void testGetAllUsers_Failed() {
        //RuntimeException actualException = assertThrows(RuntimeException.class, () -> userController.getUsers(""));
        // assertEquals("Wrong Exception message found", "Exception while finding User", actualException.getMessage());
    }

    @Test
    public void testGetUserById_Successful() {
        when(userService.getUserById(anyLong())).thenReturn(createTestUser(1L));
        //UserDto userById = userController.getAllUsers(Optional.of(1L),Optional.empty());
        // assertEquals(1L, userById.getId());
    }

    @Test
    public void testGetUserById_NotFound() {
        when(userService.getUserById(anyLong())).thenThrow(new RuntimeException("Exception while finding user"));
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userController.getUserById(1L));
        assertEquals("Wrong Exception message found", "Exception while finding user", actualException.getMessage());
    }

    @Test
    public void testGetUserByEmail_Successful() {
        when(userService.getUserById(anyLong())).thenReturn(createTestUser(1L));
        //UserDto userById = userController.getAllUsers(Optional.of(1L),Optional.empty());
        // assertEquals(1L, userById.getId());
    }

    @Test
    public void testGetUserByEmail_NotFound() {
        when(userService.getUserById(anyLong())).thenThrow(new RuntimeException("Exception while reading user"));
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userController.getUserById(1L));
        assertEquals("Wrong Exception message found", "Exception while reading user", actualException.getMessage());
    }

    @Test
    public void testUpdateUser_Successful() {
        when(userService.updateUser(any())).thenReturn(createTestUser(1L));
        //Method under Testing
//    UserDto user = userController.updateUser(userDtoTest);
//    assertEquals(Long.valueOf(1L), user.getId());
    }

    @Test
    public void testUpdateUser_NotSuccessful() {
        when(userService.updateUser(any())).thenThrow(new RuntimeException("Exception while update user"));
//        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userController.updateUser(userDtoTest));
        //Then
//        assertEquals("Wrong Exception message found", "Exception while update user", actualException.getMessage());
    }

    @Test
    public void testDeleteUser_Successful() {
        when(userService.saveUser(any())).thenReturn(createTestUser(1L));
        //Given
        UserDto user = userController.saveUser(userDtoTest);
        System.out.println(user.getId());
        //Method under Test
        userController.deleteUser(user.getId());
        //Then
        //assertNull(user.getUsername());
    }

}
