package com.qs.webserver.service;

import com.qs.webserver.dto.UserDto;
import com.qs.webserver.entity.User;
import com.qs.webserver.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    public UserRepository userRepository;

    @InjectMocks
    public UserService userService;

    public UserDto userDtoTest = new UserDto(createTestUser(1L));

    private User createTestUser(Long userId) {
        User user = new User();
        user.setName("Test");
        user.setUsername("Test1");
        user.setEmail("test1234@test.com");
        user.setPassword("test123456");
        user.setId(userId);
        return user;
    }

    @Test
    public void testSaveUser_Successful() {
        //Given
        User user = userService.saveUser(userDtoTest);
        //When
        when(userRepository.createUser(any())).thenReturn(createTestUser(1L));
        //Then
        assertEquals(userDtoTest.getUsername(), user.getUsername());
    }

    @Test
    public void testCreateUser_NotSuccessful() {
        //When
        when(userRepository.createUser(any())).thenThrow(new RuntimeException("Exception while creating user"));
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userService.saveUser(userDtoTest));
        //Then
        assertEquals("Exception while creating user", actualException.getMessage(), "Wrong Exception message found");
    }

    @Test
    public void testGetUserById() {
        //Given
        long userId = 42L;
        User user = createTestUser(userId);
        User userById = userService.getUserById(userId);
        //When
        when(userRepository.findUserById(userId)).thenReturn(user);
        //Then
        assertEquals(user.getUsername(), userById.getUsername());
    }

    @Test
    public void testGetUserByEmail() {
        //Given
        User user = createTestUser(1L);
        User userByEmail = userService.getUserByEmail(user.getEmail());
        //When
        when(userRepository.findUserByEmail(user.getEmail())).thenReturn(user);
        //Then
        assertEquals(user.getUsername(), userByEmail.getUsername());
    }

    @Test
    public void testUpdateUser_Successful() {
        //Given
        User user = userService.updateUser(userDtoTest);
        //When
        when(userRepository.updateUser(any())).thenReturn(createTestUser(1L));
        //Then
        assertEquals(userDtoTest.getUsername(), user.getUsername());
    }

    @Test
    public void testUpdateUser_NotSuccessful() {
        //When
        when(userRepository.updateUser(any())).thenThrow(new RuntimeException("Exception while update user"));
        RuntimeException actualException = assertThrows(RuntimeException.class, () -> userService.updateUser(userDtoTest));
        //Then
        assertEquals("Exception while update user", actualException.getMessage(), "Wrong Exception message");
    }

    @Test
    public void testDeleteUser_Successful() {
        //Given
        long userId = 42L;
        User user = createTestUser(userId);
        //Method under Test
        userService.saveUser(userDtoTest);
        userService.deleteUser(user);
        //Then
        assertNull(userService.getUserById(42L));
    }
}