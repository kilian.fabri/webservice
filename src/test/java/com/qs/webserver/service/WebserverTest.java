package com.qs.webserver.service;

class WebserverTest {

  /*public void testGetControllerGETMethodByPath_Successful() throws NoSuchMethodException {
    //Given
    String httpOperation = "GET";
    String requestPath = "/user/1";
    Method aMethod = UserController.class.getDeclaredMethod("getUserById", Long.class);
    //Method under Test
    Method controllerMethodByPath = applicationContext.getControllerMethodByPath(httpOperation, requestPath);
    //Then
    assertEquals("Wrong Method", controllerMethodByPath, aMethod);
  }

  @org.junit.Test
  public void testGetControllerPOSTMethodByPath_Successful() throws NoSuchMethodException {
    //Given
    String httpOperation = "POST";
    String requestPath = "/user";
    Method aMethod = UserController.class.getDeclaredMethod("saveUser", UserDto.class);
    //Method under Test
    Method controllerMethodByPath = applicationContext.getControllerMethodByPath(httpOperation, requestPath);
    //Then
    assertEquals("Wrong Method", controllerMethodByPath, aMethod);
  }

  @org.junit.Test
  public void testGetControllerPUTMethodByPath_Successful() throws NoSuchMethodException {
    //Given
    String httpOperation = "PUT";
    String requestPath = "/user/1";
    Method aMethod = UserController.class.getDeclaredMethod("updateUser", UserDto.class);
    //Method under Test
    Method controllerMethodByPath = applicationContext.getControllerMethodByPath(httpOperation, requestPath);
    //Then
    assertEquals("Wrong Method", controllerMethodByPath, aMethod);
  }

  @org.junit.Test
  public void testGetControllerDELETEMethodByPath_Successful() throws NoSuchMethodException {
    //Given
    String httpOperation = "DELETE";
    String requestPath = "/user/1";
    Method aMethod = UserController.class.getDeclaredMethod("deleteUser", Long.class);
    //Method under Test
    Method controllerMethodByPath = applicationContext.getControllerMethodByPath(httpOperation, requestPath);
    //Then
    assertEquals("Wrong Method", controllerMethodByPath, aMethod);
  }


  @Test
  public void testGetControllerGETMethodParameterByRequest_Successful() throws NoSuchMethodException {
    //Given
    String httpOperation = "GET";
    String requestPath = "/user/1";

    Method declaredMethod = UserController.class.getDeclaredMethod("getUserById", Long.class);
    Method controllerMethodByPath = applicationContext.getControllerMethodByPath(httpOperation, requestPath);

    assertEquals("Wrong Method", controllerMethodByPath, declaredMethod);
  }

  public String requestBody = "  \"name\": \"Tim\",\n" +
          "  \"email\": \"tim@quinscape.de\",\n" +
          "  \"username\": \"tim\",\n" +
          "  \"password\": \"1234\"";
  public Map<String, Object> controllerMethodParameterMap = applicationContext.extractRequestParameterFromRequestPath("GET", "/user/11");


  @Test
  public void testGetParameterToInvokeControllerMethodLong_Successful() throws NoSuchMethodException {
    //Given
    Method aMethod = ApplicationContextTest.class.getMethod("testMethodLong", Long.class);
    //Method under Test
    Object[] parameters = applicationContext.getParameterToInvokeControllerMethod(aMethod, controllerMethodParameterMap, requestBody);
    //Then
    assertEquals("Wrong parameter", parameters[0], 11L);
  }

  public void testMethodLong(@PathParameter(name = "userId") Long id) {
    //test Code
  }

  @Test
  public void getParameterToInvokeControllerMethodInt_Successful() throws NoSuchMethodException {
    //Given
    Method aMethod = ApplicationContextTest.class.getMethod("testMethodInt", Integer.class);
    //Method under Test
    Object[] parameters = applicationContext.getParameterToInvokeControllerMethod(aMethod, controllerMethodParameterMap, requestBody);
    //Then
    assertEquals("Wrong parameter", parameters[0], 11);
  }

  public void testMethodInt(@PathParameter(name = "userId") Integer id) {
    //test Code
  }


  @Test
  public void getParameterToInvokeControllerMethodString_Successful() throws NoSuchMethodException {
    //Given
    Method aMethod = ApplicationContextTest.class.getMethod("testMethodString", String.class);
    //Method under Test
    Object[] parameters = applicationContext.getParameterToInvokeControllerMethod(aMethod, controllerMethodParameterMap, requestBody);
    //Then
    assertEquals("Wrong parameter", parameters[0], "11");
  }

  public void testMethodString(@PathParameter(name = "userId") String id) {
    //test Code
  }

  @Test
  public void getParameterToInvokeControllerMethod_CouldNotParse() throws NoSuchMethodException {
    //Given
    Map<String, Object> controllerMethodParameterMap = applicationContext.extractRequestParameterFromRequestPath("GET", "/user/user");
    Method aMethod = ApplicationContextTest.class.getMethod("testMethodLong", Long.class);
    //Method under Test
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.getParameterToInvokeControllerMethod(aMethod, controllerMethodParameterMap, requestBody));
    Assertions.assertEquals("Could not Parse user to Integer", actualException.getMessage(), "Wrong Exception message found");
  }

  @Test
  public void getParameterToInvokeControllerMethod_UnknownParameter() throws NoSuchMethodException {
    //Given
    Map<String, Object> controllerMethodParameterMap = applicationContext.extractRequestParameterFromRequestPath("GET", "/user/11");
    Method aMethod = ApplicationContextTest.class.getMethod("testMethodUnknownParameter", Long.class);
    //Method under Test
    RuntimeException actualException = assertThrows(RuntimeException.class, () -> applicationContext.getParameterToInvokeControllerMethod(aMethod, controllerMethodParameterMap, requestBody));
    Assertions.assertEquals("Unknown Parameter: type=java.lang.Long, name=arg0", actualException.getMessage(), "Wrong Exception message found");
  }

  public void testMethodUnknownParameter(Long id) {
    //test Code
  }*/
}