package com.qs.webserver;

import com.qs.webserver.framework.ApplicationFramework;

public class Main {
  // Main dann über Package
  private static ApplicationFramework applicationFramework;

  public static void main(String[] args) {
    applicationFramework = new ApplicationFramework(Main.class);
  }
}
