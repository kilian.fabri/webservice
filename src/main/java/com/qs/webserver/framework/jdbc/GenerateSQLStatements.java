package com.qs.webserver.framework.jdbc;

import com.qs.webserver.framework.annotation.*;
import com.qs.webserver.framework.context.ApplicationContext;
import com.qs.webserver.framework.repository.CrudRepository;
import com.qs.webserver.framework.util.logger.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GenerateSQLStatements {

  @Autowired
  private ApplicationContext applicationContext;

  public final Logger logger = Logger.getLogger(GenerateSQLStatements.class);

  private String findByEmailSQLStatement = "";
  private String findByIdSQLStatement = "";
  private String saveUserSQlStatement = "";
  private String createTableSQLStatement = "";
  private String findAllSQLStatement = "";
  private String countSQLStatement = "";
  private String deleteByIdSQLStatement = "";

  public void generateAllSQLStatements() {
    applicationContext.getRegisteredRepositories()
            .forEach(aRepoClass -> {
              logger.info("Repository Class: %s found", aRepoClass.getSimpleName());

              Type[] interfaceTypes = getTypeArgumentsFromRepositoryClass(aRepoClass);
              Class<?> entityClass = (Class<?>) interfaceTypes[0];
              Class<? extends Comparable<?>> idClass = (Class<? extends Comparable<?>>) interfaceTypes[1];

              if (!entityClass.isAnnotationPresent(Table.class)) {
                throw new IllegalArgumentException(String.format("The found entity doesn't declare %s annotations", Table.class));
              }

              generateCreateTableStatement(entityClass, idClass);

              Arrays.stream(aRepoClass.getMethods())
                      .forEach(aMethod -> {
                        if (aMethod.getName().equals("save")) {
                          generateSaveEntitySQLStatement(entityClass, aMethod);
                        }
                        if (aMethod.getName().equals("findById")) {
                          generateFindByIdStatement(entityClass, aMethod);
                        }
                        if (aMethod.getName().equals("findByEmail")) {
                          generateFindByEmailStatement(entityClass, aMethod);
                        }
                        if (aMethod.getName().equals("deleteById")) {
                          generateDeleteByIdStatement(entityClass, aMethod);
                        }
                        if (aMethod.getName().equals("findAll")) {
                          generateFindAllStatement(entityClass, aMethod);
                        }
                        if (aMethod.getName().equals("count")) {
                          generateCountStatement(entityClass, aMethod);
                        }
                      });
            });
  }

  public Type[] getTypeArgumentsFromRepositoryClass(Class<?> repositoryClass) {
    Optional<Class<?>> classStream = Arrays
            .stream(repositoryClass.getInterfaces())
            .filter(aClass -> aClass == CrudRepository.class)
            .findFirst();

    if (classStream.isEmpty()) {
      throw new RuntimeException(String.format("%s is not a Crude Repository", repositoryClass.getInterfaces()));
    }

    Type[] genericInterfaces = repositoryClass.getGenericInterfaces();

    if (genericInterfaces.length != 1) {
      throw new IllegalArgumentException("Unknown number of generic interfaces found:" + genericInterfaces.length);
    }

    return ((ParameterizedType) genericInterfaces[0]).getActualTypeArguments();
  }

  private void generateCreateTableStatement(Class<?> entityClass, Class<? extends Comparable<?>>
          idClass) {
    final StringBuilder createTableStatementStringBuilder = new StringBuilder();
    String tableName = entityClass.getAnnotation(Table.class).table_name();

    Arrays.stream(entityClass.getDeclaredFields())
            .forEach(aField -> {
              if (aField.isAnnotationPresent(Column.class)) {
                String columnName = aField.getAnnotation(Column.class).column_name();

                if (aField.isAnnotationPresent(Id.class)) { //ToDo What do? Id.class or idClass
                  createTableStatementStringBuilder.append(columnName)
                          .append(" SERIAL PRIMARY KEY");
                } else if (aField.getType().getSimpleName().equals("String")) {
                  createTableStatementStringBuilder.append(", \n")
                          .append(columnName)
                          .append(" VARCHAR");
                } else if (aField.getType().equals(idClass)) {
                  createTableStatementStringBuilder.append(", \n")
                          .append(columnName)
                          .append(" BIGINT");
                } else {
                  createTableStatementStringBuilder.append(", \n")
                          .append(columnName)
                          .append(" ")
                          .append(aField.getType().getSimpleName().toUpperCase());
                }
                createTableStatementStringBuilder.append(" NOT NULL");
              }
              if (aField.isAnnotationPresent(Constraint.class)) {
                String constraintName = aField.getAnnotation(Constraint.class).constraint_name().toUpperCase();
                createTableStatementStringBuilder.append(" ")
                        .append(constraintName);
              }
            });
    createTableSQLStatement = String.format("CREATE TABLE IF NOT EXISTS public.%s\n(\n%s\n)\nTABLESPACE pg_default;", tableName, createTableStatementStringBuilder);
  }

  public String getCreateTableStatement() {
    return createTableSQLStatement;
  }

  private void generateSaveEntitySQLStatement(Class<?> entityClass, Method aMethod) {
    String tableName = entityClass.getAnnotation(Table.class).table_name();
    List<String> columnNames = Arrays.stream(entityClass.getDeclaredFields())
            .filter(field -> field.isAnnotationPresent(Column.class) && !field.isAnnotationPresent(Id.class))
            .map(Field::getName)
            .collect(Collectors.toList());

    String columnValuePlaceholder = columnNames.stream().map(s -> "?").collect(Collectors.joining(", "));
    saveUserSQlStatement = String.format("INSERT INTO %s (%s) VALUES (%s);", tableName, String.join(", ", columnNames), columnValuePlaceholder);
  }

  public String getSaveUserSQlStatement() {
    return saveUserSQlStatement;
  }

  private void generateFindByIdStatement(Class<?> entityClass, Method aMethod) {
    String tableName = entityClass.getAnnotation(Table.class).table_name();

    Optional<String> columnId = Arrays.stream(entityClass.getDeclaredFields())
            .filter(aField -> aField.isAnnotationPresent(Id.class))//ToDo What do? Id.class or idClass
            .map(Field::getName)
            .findAny();

    if (columnId.isEmpty()) {
      logger.error("No Id Field found for given Table Class Operation=%s", entityClass);
      throw new RuntimeException(String.format("No Id Field found for given Table Class Operation=%s", entityClass));
    }

    findByIdSQLStatement = String.format("SELECT * FROM %s WHERE %s = ?", tableName, columnId.get());
  }

  public String getFindByIdSQLStatement() {
    return findByIdSQLStatement;
  }

  private void generateFindByEmailStatement(Class<?> entityClass, Method aMethod) {
    String tableName = entityClass.getAnnotation(Table.class).table_name();

    Optional<String> columnEmail = Arrays.stream(entityClass.getDeclaredFields())
            .filter(aField -> aField.getAnnotation(Column.class).column_name().contains("email"))
            .map(Field::getName)
            .findAny();

    if (columnEmail.isEmpty()) {
      logger.error("No Email Field found for given Table Class Operation=%s", entityClass);
      throw new RuntimeException(String.format("No Email Field found for given Table Class Operation=%s", entityClass));
    }

    findByEmailSQLStatement = String.format("SELECT * FROM %s WHERE %s = ?", tableName, columnEmail.get());
  }

  public String getFindByEmailSQLStatement() {
    return findByEmailSQLStatement;
  }

  private void generateDeleteByIdStatement(Class<?> entityClass, Method aMethod) {
    String tableName = entityClass.getAnnotation(Table.class).table_name();

    Optional<String> columnId = Arrays.stream(entityClass.getDeclaredFields())
            .filter(aField -> aField.getAnnotation(Column.class).column_name().contains("id"))
            .map(Field::getName)
            .findAny();

    if (columnId.isEmpty()) {
      logger.error("No ID Field found for given Table Class Operation=%s", entityClass);
      throw new RuntimeException(String.format("No ID Field found for given Table Class Operation=%s", entityClass));
    }

    deleteByIdSQLStatement = String.format("DELETE FROM %s WHERE %s = ?", tableName, columnId.get());
  }

  public String getDeleteByIdSQLStatement() {
    return deleteByIdSQLStatement;
  }

  private void generateFindAllStatement(Class<?> entityClass, Method aMethod) {
    String tableName = entityClass.getAnnotation(Table.class).table_name();
    findAllSQLStatement = String.format("SELECT * FROM %s", tableName);
  }

  public String getFindAllSQLStatement() {
    return findAllSQLStatement;
  }

  private void generateCountStatement(Class<?> entityClass, Method aMethod) {
    String tableName = entityClass.getAnnotation(Table.class).table_name();
    countSQLStatement = String.format("SELECT COUNT(*) AS count FROM %s", tableName);

  }

  public String getCountStatement() {
    return countSQLStatement;
  }
}
