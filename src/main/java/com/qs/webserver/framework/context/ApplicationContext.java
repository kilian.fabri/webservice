package com.qs.webserver.framework.context;

import com.qs.webserver.framework.annotation.*;
import com.qs.webserver.framework.exception.BeanAlreadyExistsException;
import com.qs.webserver.framework.exception.BeanNotFoundException;
import com.qs.webserver.framework.jdbc.GenerateSQLStatements;
import com.qs.webserver.framework.repository.CrudRepository;
import com.qs.webserver.framework.util.Property;
import com.qs.webserver.framework.util.logger.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ApplicationContext {

  private final ApplicationClassLoader applicationClassLoader;
  @Autowired
  GenerateSQLStatements sqlStatements;

  public final Logger logger = Logger.getLogger(ApplicationContext.class);

  private final Properties properties;

  private final Map<String, Object> applicationContextMap = new HashMap<>();

  private final Map<String, Method> applicationFactoriesMap = new HashMap<>();

  private final Map<String, Method> controllerMethodsMap = new HashMap<>();

  private final Set<Object> runnableServicesSet = new HashSet<>();

  private final Set<Class<?>> repositorySet = new HashSet<>();

  public ApplicationContext(String rootPackageName) {
    this.properties = Property.loadProperties();
    this.applicationClassLoader = new ApplicationClassLoader(rootPackageName);
    init();
    injectProperties();
    sqlStatements.generateAllSQLStatements();
  }

  /**
   * Checks all Classes if an Annotation is present and invokes the matching {@link Method Register Method}<br>
   * If the Class is this Class put it directly into {@link Map applicationContextMap}
   */
  private void init() {
    try {//TODO ? Über getClassesByAnnotation oder so lassen
      applicationClassLoader.findAllClasses().values()
              .forEach(aClass -> {
                if (aClass.isAnnotationPresent(Service.class) && !aClass.getSimpleName().equals(this.getClass().getSimpleName())) {
                  registerBean(aClass);
                  logger.info("Service Class: %s was initialised", aClass.getSimpleName());
                }
                if (aClass.isAnnotationPresent(Factory.class)) {
                  registerFactory(aClass);
                  logger.info("Factory Class: %s was initialised", aClass.getSimpleName());
                }
                if (aClass.isAnnotationPresent(Controller.class)) {
                  registerControllerMethods(aClass);
                  registerBean(aClass);
                  logger.info("Controller Class: %s was initialised", aClass.getSimpleName());
                }
                if (aClass.isAnnotationPresent(RunnableService.class)) {
                  registerBean(aClass);
                  registerRunnable(aClass);
                  logger.info("Runnable Class: %s was initialised", aClass.getSimpleName());
                }
                if (aClass.isAnnotationPresent(Repository.class)) {
                  registerRepository(aClass);
                  logger.info("Repository: %s was initialised", aClass.getSimpleName());
                }
              });
      //Register the ApplicationContext to make it Injectable
      applicationContextMap.put(this.getClass().getCanonicalName(), this);
      logger.info("Class: %s was registered", this.getClass().getSimpleName());
    } catch (NullPointerException e) {
      throw new RuntimeException("Exception in Method init", e);
    }
  }

  /////Bean//////////////////////////////////////////////////////////////////////////////////////

  /**
   * Register the {@link Class beanClass} and put it in {@link Map applicationContextMap}
   *
   * @param beanClass the Class which should be registered
   */
  public void registerBean(Class<?> beanClass) {
    if (applicationContextMap.get(beanClass.getCanonicalName()) != null) {
      logger.error("BeanAlreadyExistsException in registerBean method");
      throw new BeanAlreadyExistsException(String.format("Bean %s already exists", beanClass.getSimpleName()));
    }

    try {
      Object bean = beanClass.getDeclaredConstructor(null).newInstance(null);
      applicationContextMap.put(beanClass.getCanonicalName(), bean);
      logger.info(String.format("Registered bean: %s", beanClass.getSimpleName()));
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      logger.error("Could not Register Bean: ", beanClass.getSimpleName(), e);
      throw new RuntimeException("Could not register Bean " + beanClass.getSimpleName(), e);
    }
  }

  /**
   * Get Bean matching {@link String beanName} from {@link Map applicationContextMap}
   *
   * @param beanName    name of the which bean should be gotten
   * @param clazz       {@link Class}
   * @param isMandatory {@link Boolean}
   * @param <T>
   * @return {@link Object} matching {@link String beanName}
   */
  public <T> T getBean(String beanName, Object clazz, boolean isMandatory) {
    if (beanName == null) {
      logger.error("IllegalArgumentException in getBean method");
      throw new IllegalArgumentException("beanName is null");
    }
    Object bean = applicationContextMap.get(beanName);
    if (isMandatory && bean == null) {
      logger.error("BeanNotFoundException in getBean method");
      throw new BeanNotFoundException(String.format("Could not find bean %s", beanName));
    }
    if (bean.getClass() != clazz) {
      logger.error("IllegalArgumentException in getBean method");
      throw new IllegalArgumentException(String.format("%s and %s are not equal", bean.getClass(), clazz));
    }

    return (T) bean;
  }

  /**
   * Remove Bean matching {@link String beanName} from {@link Map applicationContextMap}
   *
   * @param beanName Bean which should be removed
   */
  public void unregisterBean(String beanName) {
    applicationContextMap.remove(beanName);
    logger.info(String.format("Removed bean: %s", beanName));
  }

  ////////Factory//////////////////////////////////////////

  /**
   * Register the {@link Class factoryClass} and put it in {@link Map applicationFactoriesMap}
   *
   * @param factoryClass the Class which should be registered
   */
  public void registerFactory(Class<?> factoryClass) {
    try {
      Set<Method> factorySet = Arrays.stream(factoryClass.getDeclaredMethods())
              .filter(m -> m.isAnnotationPresent(Produces.class))
              .collect(Collectors.toSet());
      if (factorySet.isEmpty()) {
        logger.error("IllegalArgumentException in register Factory. %s doesn't contain any produces ", factoryClass.getCanonicalName());
        throw new IllegalArgumentException("The given FactoryClass doesn't contain any produces " + factoryClass.getCanonicalName());
      }

      Object factory = factoryClass.getDeclaredConstructor(null).newInstance(null);
      applicationContextMap.put(factoryClass.getCanonicalName(), factory);
      factorySet.forEach(method -> applicationFactoriesMap.put(method.getReturnType().getCanonicalName(), method));

    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      logger.error("Could not Register Factory: ", factoryClass.getSimpleName(), e);
      throw new RuntimeException("Could not register Factory" + factoryClass.getCanonicalName(), e);
    }
  }

  ///////////Runnable/////////////////////////////////////////////////////////////////////////////////////////////////
  private void registerRunnable(Class<?> aClass) {
    runnableServicesSet.add(getBean(aClass.getCanonicalName(), aClass, true));
  }

  public Set<Object> getRunnableServicesSet() {
    return runnableServicesSet;
  }

  /////////Repository/////////////////////////////////////////////////////////////////////////////////////////////////
  private void registerRepository(Class<?> aClass) {
    if (!aClass.isInterface()) {
      throw new RuntimeException("The given repository is not an interface: " + aClass.getCanonicalName());
    }
    if (Arrays.stream(aClass.getInterfaces()).noneMatch(aClass1 -> aClass1 == CrudRepository.class)) {
      throw new RuntimeException("The given repository does not implement CrudRepository: " + aClass.getCanonicalName());
    }
    if (repositorySet.contains(aClass)) {
      throw new RuntimeException("The given repository was already registered: " + aClass.getCanonicalName());
    }
    repositorySet.add(aClass);
  }

  public Set<Class<?>> getRegisteredRepositories() {
    return repositorySet;
  }

  ///////Inject Properties//////////////////////////////////////////////////////////////////////////////////////////
  public void injectProperties() {
    applicationContextMap.values()
            .forEach(aService -> Arrays.stream(aService.getClass().getDeclaredFields())
                    .forEach(aField -> {
                      setFieldWherePropertyValueAnnotation(aService, aField);
                      setFieldWhereAutowiredAnnotation(aService, aField);
                    }));
  }

  /**
   * Set Field with an instance of a Class where {@link Autowired} is present
   *
   * @param aService an instance of a Class from {@link Map applicationContextMap}
   * @param aField   The Field which should be set
   */
  private void setFieldWhereAutowiredAnnotation(Object aService, Field aField) {
    try {
      if (aField.isAnnotationPresent(Autowired.class)) {//If there is a Field where the Annotation Autowired present
        aField.setAccessible(true);
        if (applicationContextMap.containsKey(aField.getType().getName())) { //if the map applicationContext contains / there is a bean with the same name of a Field
          aField.set(aService, applicationContextMap.get(aField.getType().getName())); //sets the service and the bean with the same name of a Field into the field
          logger.info("Autowired : " + applicationContextMap.get(aField.getType().getName()) + " was set into Field " + aField);
        } else if (applicationFactoriesMap.containsKey(aField.getType().getCanonicalName())) { //if the map applicationFactories contains the same name of a Field
          Method factoryMethod = applicationFactoriesMap.get(aField.getType().getCanonicalName());  //factoryMethod is declared as Method and initialised with a method from the applicationFactories map which contains the name of a Field
          Object factory = applicationContextMap.get(factoryMethod.getDeclaringClass().getCanonicalName()); //factory is declared as Object and initialised with object from the applicationContext map which contains the name of the Class object representing the class that declares the method represented by this object
          Object requiredClass = factoryMethod.invoke(factory, null); //requiredClass is declared as Object and initialised with the invoked factoryMethode from factory -> which is the class name and null arguments because the methode has non
          aField.set(aService, requiredClass);//sets the service and the Object requiredClass into the field
          logger.info("Connection was created and set into Field " + aField);
        }
      }
    } catch (Exception e) {
      String message = String.format("Exception while Autowiring field. Service = %s, Field %s", aService.getClass().getCanonicalName(), aField.getName());
      logger.error(message, e);
      throw new RuntimeException(message, e);
    }
  }

  /**
   * Set Field with the needed Properties where {@link PropertyValue} is present
   *
   * @param aService an instance of a Class from {@link Map applicationContextMap}
   * @param aField   The Field which should be set
   */
  private void setFieldWherePropertyValueAnnotation(Object aService, Field aField) {
    if (aField.isAnnotationPresent(PropertyValue.class)) {
      PropertyValue propertyValue = aField.getAnnotation(PropertyValue.class);
      aField.setAccessible(true);
      try {
        aField.set(aService, properties.getProperty(propertyValue.name()));
        logger.info("Property Value: " + properties.getProperty(propertyValue.name()) + " was set into Field " + aField);
      } catch (IllegalAccessException e) {
        logger.error("IllegalAccessException while setting Properties into Field", e);
        throw new RuntimeException("IllegalAccessException while setting Properties into Field", e);
      }
    }
  }

  ///////Controller Method//////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Register all Methods from the given Class where a {@link Controller Controller Annotation}
   *
   * @param aClass Class from which the Methods should be registered
   */
  private void registerControllerMethods(Class<?> aClass) {
    Controller controller = aClass.getAnnotation(Controller.class);
    Arrays.stream(aClass.getDeclaredMethods())
            .filter(method -> method.isAnnotationPresent(PostOperation.class)
                    || method.isAnnotationPresent(GetOperation.class)
                    || method.isAnnotationPresent(PutOperation.class)
                    || method.isAnnotationPresent(DeleteOperation.class))
            .forEach(aMethod -> {
              String methodPath = "";
              if (aMethod.isAnnotationPresent(PostOperation.class)) {
                PostOperation postOperation = aMethod.getAnnotation(PostOperation.class);
                methodPath = "POST=" + controller.path() + postOperation.path();
              }
              if (aMethod.isAnnotationPresent(GetOperation.class)) {
                GetOperation getOperation = aMethod.getAnnotation(GetOperation.class);
                methodPath = "GET=" + controller.path() + getOperation.path();
              }
              if (aMethod.isAnnotationPresent(PutOperation.class)) {
                PutOperation putOperation = aMethod.getAnnotation(PutOperation.class);
                methodPath = "PUT=" + controller.path() + putOperation.path();
              }
              if (aMethod.isAnnotationPresent(DeleteOperation.class)) {
                DeleteOperation deleteOperation = aMethod.getAnnotation(DeleteOperation.class);
                methodPath = "DELETE=" + controller.path() + deleteOperation.path();
              }
              if (controllerMethodsMap.containsKey(methodPath)) {
                throw new IllegalArgumentException("Controller Method already exists" + methodPath);
              }
              controllerMethodsMap.put(methodPath, aMethod);
            });
  }

  public Map<String, Method> getControllerMethodsMap() {
    return controllerMethodsMap;
  }

}