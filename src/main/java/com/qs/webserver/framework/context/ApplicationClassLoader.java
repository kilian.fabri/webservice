package com.qs.webserver.framework.context;

import com.qs.webserver.framework.util.logger.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ApplicationClassLoader extends ClassLoader {

  public final Logger logger = Logger.getLogger(ApplicationClassLoader.class);

  private final String rootPackageName;

  public ApplicationClassLoader(String rootPackageName) {
    this.rootPackageName = rootPackageName;
  }

  public Map<String, Class<?>> findAllClasses() {
    File rootFile = new File("./out/production/workspace/" + rootPackageName
      .replaceAll("[.]", "/"));
    Map<String, Class<?>> classMap = new HashMap<>();
    collectClasses(rootFile.getAbsolutePath(), classMap);
    return classMap;
  }

  private void collectClasses(String rootFile, Map<String, Class<?>> classes) {
    File root = new File(rootFile);
    File[] files = root.listFiles();

    if (files != null) {
      for (File file : files) {
        if (file.isDirectory()) {
          collectClasses(file.getAbsolutePath(), classes);
        }
        if (file.getPath().endsWith(".class")) {
          String canonicalClassName = getCanonicalClassName(file.getAbsolutePath());
          try {
            Class<?> foundClass = Class.forName(canonicalClassName);
            classes.put(foundClass.getCanonicalName(), foundClass);
          } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load class " + canonicalClassName);
          }
        }
      }
    }
  }

  public String getCanonicalClassName(String fileName) {
    final String operatingSystem = System.getProperty("os.name");

    if (operatingSystem == null || "".equals(operatingSystem)) {
      throw new RuntimeException("Operating system could not be determined.");
    }

    if (operatingSystem.startsWith("Windows")) {
      int rootPackageIndex = fileName.indexOf(rootPackageName.replaceAll("\\.", "\\\\"));

      return fileName
        .substring(rootPackageIndex)
        .replaceAll("\\\\", "\\.")
        .replaceAll(".class", "");
    }

    int rootPackageIndex = fileName.indexOf(rootPackageName.replaceAll("\\.", "/"));
    return fileName
      .substring(rootPackageIndex)
      .replace("/", ".")
      .replace(".class", "");
  }
}
