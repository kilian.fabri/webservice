package com.qs.webserver.framework;


import com.qs.webserver.framework.context.ApplicationContext;
import com.qs.webserver.framework.util.logger.Logger;

import java.util.concurrent.*;


public class ApplicationFramework {
  private final ApplicationContext applicationContext;

  private final Logger logger = Logger.getLogger(ApplicationFramework.class);

  private final BlockingQueue<Runnable> blockingQueue = new PriorityBlockingQueue<>();

  private final Executor executor = new ThreadPoolExecutor(2, 4, 10, TimeUnit.SECONDS, blockingQueue);//Per properties

  public ApplicationFramework(Class<?> applicationClass) {
    applicationContext = new ApplicationContext(applicationClass.getPackageName());
    run();
  }

  public void run() {
    applicationContext.getRunnableServicesSet()
      .forEach(runnableService -> {
        logger.info("Runnable Service: %s executed", runnableService.getClass().getSimpleName());
        executor.execute((Runnable) runnableService);
      });
  }
}
