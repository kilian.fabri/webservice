package com.qs.webserver.framework.repository;

import java.util.Optional;
import java.util.Set;

public interface CrudRepository<T, ID extends Comparable<?>> {
  long count();

  T save(T t);

  Optional<T> findById(ID id);

  void deleteById(ID id);

  Set<T> findAll();
}
