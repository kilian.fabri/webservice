package com.qs.webserver.framework.exception;

public class BeanAlreadyExistsException extends AlreadyExistsException {
  public BeanAlreadyExistsException(String message) {
    super(message);
  }

  public BeanAlreadyExistsException(String message, Throwable cause) {
    super(message, cause);
  }
}
