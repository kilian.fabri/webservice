package com.qs.webserver.framework.exception;

/**
 * Exception when client request contains errors.
 */
public class RequestException extends RuntimeException {
    public RequestException(String message) {
        super(message);
    }

    public RequestException(String message, Throwable cause) {
        super(message, cause);
    }
}

