package com.qs.webserver.framework.util.constants;

public class ConstantsStatusCode {
  public static final int OK_STATUS_CODE = 200;

  public static final int BAD_REQUEST_STATUS_CODE = 400;
  public static final int FORBIDDEN_STATUS_CODE = 403;
  public static final int RESOURCE_NOT_FOUND_STATUS_CODE = 404;
  public static final int NOT_ALLOWED_STATUS_CODE = 405;
  public static final int INTERNAL_SERVER_ERROR_STATUS_CODE = 500;
}
