package com.qs.webserver.framework.util.logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LoggerFormatter extends Formatter {
  // Create a DateFormat to format the logger timestamp.
  private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss.SSS");

  public String format(LogRecord record) {
    return new StringBuilder(1000)
            .append(df.format(new Date(record.getMillis()))).append(" - ")
            .append("[").append(record.getLoggerName()).append("] - ")
            .append("[").append(record.getLevel()).append("] - ")
            .append(formatMessage(record))
            .append("\n")
            .toString();
  }
}
