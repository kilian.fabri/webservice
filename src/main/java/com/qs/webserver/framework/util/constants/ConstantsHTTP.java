package com.qs.webserver.framework.util.constants;

public class ConstantsHTTP {
  public static final String HTTP_PATH = "HTTP_PATH";
  public static final String HTTP_OPERATION = "HTTP_OPERATION";
  public static final String AUTHORIZATION = "AUTHORIZATION";
}
