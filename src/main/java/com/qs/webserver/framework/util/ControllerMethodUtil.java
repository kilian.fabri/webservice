package com.qs.webserver.framework.util;

import com.qs.webserver.framework.annotation.*;
import com.qs.webserver.framework.context.ApplicationContext;
import com.qs.webserver.framework.util.logger.Logger;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ControllerMethodUtil {

  @Autowired
  ApplicationContext applicationContext;

  public static final Logger logger = Logger.getLogger(ControllerMethodUtil.class);

  /**
   * Gets a Method from controllerMethodsMap
   *
   * @return Method matching
   */
  public Method getControllerMethodByPath(String httpOperation, String requestPath) {
    if (requestPath.contains("?")) {
      String[] splitRequestPath = requestPath.split("\\?");
      requestPath = splitRequestPath[0];
    }

    String controllerMethodKey = getControllerMethodKey(httpOperation, requestPath);
    Map<String, Method> controllerMethodsMap = applicationContext.getControllerMethodsMap();
    return controllerMethodsMap.get(controllerMethodKey);
  }

  /**
   * Get the Key Value from Map with all Controller Methods matching the given httpOperation and requestPath
   *
   * @param httpOperation
   * @param requestPath
   * @return a Key Value from Map with all Controller Methods
   */
  private String getControllerMethodKey(String httpOperation, String requestPath) {
    Map<String, Method> controllerMethodsMap = applicationContext.getControllerMethodsMap();
    Optional<String> optionalControllerMethodKey = controllerMethodsMap
            .keySet()
            .stream()
            .filter(methodKey -> {
              String[] controllerMethodTokens = methodKey.split("=")[1].split("/");
              String[] requestPathTokens = requestPath.split("/");
              if (!methodKey.startsWith(httpOperation) || controllerMethodTokens.length != requestPathTokens.length) {

                return false;
              }

              for (int i = 0; i < controllerMethodTokens.length; i++) {
                if (!(controllerMethodTokens[i].equals(requestPathTokens[i]))) {
                  if (!(controllerMethodTokens[i].startsWith("{") && controllerMethodTokens[i].endsWith("}"))) {
                    //Token does not match and not parameter
                    return false;
                  }
                }
              }
              return true;
            })
            .findFirst();

    if (optionalControllerMethodKey.isEmpty()) {
      logger.error("No Method found for given Request Operation=%s or Request Path=%s", httpOperation, requestPath);

      throw new RuntimeException(String.format("No Method found for given Request Operation=%s or Request Path=%s", httpOperation, requestPath));
    }

    return optionalControllerMethodKey.get();
  }

  public List<String> extractQueryParameterFromRequestPath(String queryParameterValues) {
    List<String> requestPathList = new ArrayList<>();
    if (queryParameterValues.contains("&")) {
      requestPathList = Arrays.stream(queryParameterValues.split("&")).collect(Collectors.toList());
    } else {
      requestPathList.add(queryParameterValues);
    }
    return requestPathList;
  }

  /**
   * Get Parameter Name and Values for the Controller Method matching the giving Operation and Path
   *
   * @param httpOperation
   * @param requestPath
   * @return Map with Parameter Name and Value
   */
  public Map<String, Object> extractPathParameterFromRequestPath(String httpOperation, String requestPath) {
    Map<String, Object> pathParameterMap = new HashMap<>();

    String controllerMethodKey = getControllerMethodKey(httpOperation, requestPath);

    String[] requestPathTokens = requestPath.split("/");
    String[] controllerMethodKeyTokens = controllerMethodKey.split("=")[1].split("/");

    for (int i = 0; i < controllerMethodKeyTokens.length; i++) {
      if (!(controllerMethodKeyTokens[i].equals(requestPathTokens[i]))) {
        if (controllerMethodKeyTokens[i].startsWith("{")) {
          String parameterName = controllerMethodKeyTokens[i].replace("{", "").replace("}", "");
          pathParameterMap.put(parameterName, requestPathTokens[i]);
        }
      }
    }
    return pathParameterMap;
  }

  /**
   * Get the Parameter needed to invoke the given Method
   *
   * @param controllerMethod the Method which should be invoked
   * @param pathParameterMap a Map with all Parameter needed to Invoke the Method if {@link PathParameter} Annotation present
   * @return the {@link java.lang.reflect.Parameter Parameter} needed to invoke the given {@link Method controllerMethod}
   */
  public Object[] getParameterToInvokeControllerMethod(Method controllerMethod, List<String> requestParameterList, Map<String, Object> pathParameterMap, String requestBody) {
    return Arrays.stream(controllerMethod.getParameters())
            .map(aParameter -> {
              if (aParameter.isAnnotationPresent(RequestBody.class)) {

                return RequestUtil.fromJson(requestBody, aParameter.getType());
              } else if (aParameter.isAnnotationPresent(PathParameter.class)) {

                String parameter = (String) pathParameterMap.get(aParameter.getAnnotation(PathParameter.class).name());
                // Determine the type of the parameter and cast the value accordingly
                try {
                  if (aParameter.getType().equals(Long.class) || aParameter.getType().equals(long.class)) {

                    return Long.parseLong(parameter);
                  } else if (aParameter.getType().equals(Integer.class) || aParameter.getType().equals(int.class)) {

                    return Integer.parseInt(parameter);
                  } else if (aParameter.getType().equals(String.class)) {

                    return (parameter);
                  }
                } catch (NumberFormatException e) {
                  throw new RuntimeException(String.format("Could not Parse %s to Integer", parameter), e);
                }
              } else if (aParameter.isAnnotationPresent(QueryParameter.class)) {
                return requestParameterList.stream()
                        .filter(aRequestPath -> aRequestPath.contains(aParameter.getAnnotation(QueryParameter.class).name()))
                        .map(aRequestPath -> aRequestPath.split("=")[1])
                        .collect(Collectors.toList());
              } else {
                throw new IllegalArgumentException(String.format("Unknown Parameter: type=%s, name=%s", aParameter.getType().getCanonicalName(), aParameter.getName()));
              }
              throw new RuntimeException();
            }).toArray();
  }
}
