package com.qs.webserver.framework.util;

import com.qs.webserver.framework.util.logger.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Property {
  public static final Logger logger = Logger.getLogger(Property.class);

  private static final Properties properties = new Properties();

  public static Properties loadProperties() {
    try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties")) {
      try {
        properties.load(inputStream);
        //logger.info("Property from"+);
      } catch (IOException e) {
        logger.error("IOException", e);
        throw new IOException("Exception while loading properties");
      }
    } catch (IOException e) {
      logger.error("IOException", e);
      e.printStackTrace();
    }

    return properties;
  }
}
