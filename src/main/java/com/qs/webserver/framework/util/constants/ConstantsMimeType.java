package com.qs.webserver.framework.util.constants;

public class ConstantsMimeType {
  public static final String MIME_TYPE_JSON = "application/json";
  public static final String MIME_TYPE_TEXT = "application/text";
}
