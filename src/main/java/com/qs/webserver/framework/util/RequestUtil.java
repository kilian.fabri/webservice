package com.qs.webserver.framework.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qs.webserver.dto.UserDto;
import com.qs.webserver.framework.annotation.PropertyValue;
import com.qs.webserver.framework.exception.RequestException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;


public class RequestUtil {
    private static final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    @PropertyValue(name = "app.webserver.host")
    private static String webserverHost;

    @PropertyValue(name = "app.webserver.port")
    private static String webserverPort;

    /**
     * Give Response and write Message to {@link java.net.Socket}
     *
     * @param outputStream Output Stream from Webserver Socket
     * @param statusCode   Status Code of Webserver Socket
     * @param status       the Status of the Webserver Socket
     * @param mimeType     Content Type of Webserver Socket
     * @param message      The message of Webserver Socket
     */
    public static void creatResponse(OutputStream outputStream, int statusCode, String status, String mimeType, String message) {
        PrintWriter printWriter = new PrintWriter(outputStream);
        printWriter.println(String.format("HTTP/1.1 %s %s", statusCode, status));
        printWriter.println(String.format("Via: HTTP/1.1 %s:%s", webserverHost, webserverPort));
        printWriter.println("Server: Java/11");
        printWriter.println("Content-type: " + mimeType);
        int length = message == null ? 0 : message.length();
        printWriter.println("Content-length: " + length);
        printWriter.println("Access-Control-Allow-Headers: *");
        printWriter.println("Access-Control-Allow-Origin: *");
        printWriter.println("Access-Control-Allow-Methods: *");
        printWriter.println("");
        printWriter.println(message);
        printWriter.println("");
        printWriter.flush();
    }

    /**
     * Read body of Webserver Socket
     *
     * @param bufferedReader with the body of InputStream from Socket
     * @return a {@link UserDto} read from the body of Socket
     */
    public static String getRequestBody(BufferedReader bufferedReader, int contentLength) {
        try {
            StringBuilder body = new StringBuilder();
            int readLength = 0;
            while (bufferedReader.ready()) {
                body.append((char) bufferedReader.read());
                readLength++;
                if (readLength > contentLength) {
                    throw new RequestException(String.format("Content length too big. Expected = %s, Read = %s", contentLength, readLength));
                }
            }
            if (readLength < contentLength) {
                throw new RequestException(String.format("Content length insufficient. Expected = %s, Read = %s", contentLength, readLength));
            }

            return body.toString();
        } catch (IOException exception) {
            throw new RuntimeException("Could not parse request.", exception);
        }
    }

    /**
     * Turn a json and a Class into a {@link Gson}
     *
     * @param json  the String which should be transformed into {@link Gson}
     * @param clazz the Class needed to transformed into {@link Gson}
     * @param <T>
     * @return the transformed {@link Gson}
     */
    public static <T> T fromJson(String json, Class<T> clazz) {

        return gson.fromJson(json, clazz);
    }

    /**
     * Turn an Object and Class / {@link Gson}  to json
     *
     * @param object Object which should be transformed into json
     * @param clazz  the given Class needed to transformed into json
     * @return the transformed json
     */
    public static String toJson(Object object, Class<?> clazz) {

        return gson.toJson(object, clazz);
    }
}
