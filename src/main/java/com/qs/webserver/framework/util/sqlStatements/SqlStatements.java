package com.qs.webserver.framework.util.sqlStatements;

import com.qs.webserver.framework.annotation.Autowired;
import com.qs.webserver.framework.annotation.Service;
import com.qs.webserver.framework.context.ApplicationContext;

@Service
public class SqlStatements {
    public static final String SQL_UPDATE_USER = "UPDATE ws_user SET name = ?, email = ?, username = ?, password = ? WHERE id = ?";
    public static final String TEST_SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS public.ws_user4" +
                    "(" +
                    "id INT auto_increment," +
                    "name VARCHAR(20)     NOT NULL," +
                    "email VARCHAR(20)     NOT NULL UNIQUE," +
                    "username VARCHAR(20)     NOT NULL UNIQUE," +
                    "password VARCHAR(20)     NOT NULL," +
                    "PRIMARY KEY (id)" +
                    ");";
    public static final String TEST_SQL_INSERT_USER = "INSERT INTO ws_user4" +
            " (name, email, username, password) VALUES " +
            "(?,?,?,?);";
    public static final String TEST_SQL_UPDATE_USER = "UPDATE ws_user4 SET name = ?, email = ?, username = ?, password = ? WHERE id = ?";
    public static final String TEST_SQL_FIND_USER_BY_ID = "SELECT * FROM ws_user4 WHERE id = ?";
    public static final String TEST_SQL_FIND_USER_BY_EMAIL = "SELECT * FROM ws_user4 WHERE email = ?";
    public static final String TEST_SQL_DELETE_USER_BY_ID = "DELETE FROM ws_user4 WHERE id = ?";
    @Autowired
    private ApplicationContext applicationContext;

}


