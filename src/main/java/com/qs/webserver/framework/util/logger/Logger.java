package com.qs.webserver.framework.util.logger;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.logging.ConsoleHandler;


public class Logger {
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_YELLOW = "\u001B[33m";

  private final java.util.logging.Logger logger;

  public static Logger getLogger(Class clazz) {

    return new Logger(clazz);
  }

  public Logger(Class clazz) {
    this.logger = java.util.logging.Logger.getLogger(clazz.getCanonicalName());
    this.logger.setUseParentHandlers(false);
    ConsoleHandler handler = new ConsoleHandler();
    handler.setFormatter(new LoggerFormatter());
    this.logger.addHandler(handler);
  }

  public void info(String message) {
    logger.info(ANSI_GREEN + message + ANSI_RESET);
  }

  public void info(String message, Object... args) {
    info(String.format(message, args));
  }

  public void error(String message) {
    logger.severe(ANSI_RED + message + ANSI_RESET);
  }

  public void error(String message, Object... args) {
    error(String.format(message, args));
  }

  public void error(String message, Throwable throwable) {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    PrintStream printStream = new PrintStream(bos);
    throwable.printStackTrace(printStream);
    String logMessage = String.format("%s:\n%s", message, bos.toString(StandardCharsets.UTF_8));
    logger.severe(logMessage);
  }

  public void warn(String message) {
    logger.warning(ANSI_YELLOW + message + ANSI_RESET);
  }

  public void warn(String message, Object... args) {
    warn(String.format(message, args));
  }
}
