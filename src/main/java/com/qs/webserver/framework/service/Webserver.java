package com.qs.webserver.framework.service;

import com.qs.webserver.framework.annotation.Autowired;
import com.qs.webserver.framework.annotation.PropertyValue;
import com.qs.webserver.framework.annotation.RunnableService;
import com.qs.webserver.framework.context.ApplicationContext;
import com.qs.webserver.framework.exception.*;
import com.qs.webserver.framework.util.ControllerMethodUtil;
import com.qs.webserver.framework.util.RequestUtil;
import com.qs.webserver.framework.util.logger.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

import static com.qs.webserver.framework.util.constants.ConstantsHTTP.*;
import static com.qs.webserver.framework.util.constants.ConstantsMimeType.MIME_TYPE_JSON;
import static com.qs.webserver.framework.util.constants.ConstantsMimeType.MIME_TYPE_TEXT;
import static com.qs.webserver.framework.util.constants.ConstantsStatusCode.*;

@RunnableService
public class Webserver implements Runnable {

    public static final Logger logger = Logger.getLogger(Webserver.class);
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    ControllerMethodUtil controllerMethodUtil;
    @PropertyValue(name = "app.webserver.admin")
    private String webserverAdmin;

    @PropertyValue(name = "app.webserver.adminPasswort")
    private String webserverAdminPassword;

    @PropertyValue(name = "app.webserver.host")
    private String webserverHost;

    @PropertyValue(name = "app.webserver.port")
    private String webserverPort;

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(Integer.parseInt(webserverPort), 10, InetAddress.getByName(webserverHost))) {
            while (!false) {
                logger.info("Accepting Connection");

                Socket socket = serverSocket.accept();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                Map<String, String> headerMap = getHeader(bufferedReader);
                int headerMapContentLength = 0;
                if (headerMap.get("Content-Length") != null) {
                    headerMapContentLength = Integer.parseInt(headerMap.get("Content-Length"));
                }
                final OutputStream socketOutputStream = socket.getOutputStream();
                final String requestOperation = headerMap.get(HTTP_OPERATION);
                final String requestPath = headerMap.get(HTTP_PATH);

                String requestBody = RequestUtil.getRequestBody(bufferedReader, headerMapContentLength);

                // checkAuthorization(headerMap.get(AUTHORIZATION));
                processRequest(requestOperation, requestPath, requestBody, socketOutputStream);

                logger.info(headerMap.toString());
                socket.close();
            }
        } catch (IOException e) {
            logger.error("IOException", e);
        }
    }


    /**
     * Invokes a {@link Method controllerMethod}
     * by getting the name from the given Map, the Class of this Method, a Bean from that Class
     * and all Parameter needed to invoke.
     */
    private void processRequest(String httpOperation, String requestPath, String requestBody, OutputStream socketOutputStream) {
        try {
            if (httpOperation.equals("OPTIONS")) {
                RequestUtil.creatResponse(socketOutputStream, OK_STATUS_CODE, "OK", MIME_TYPE_JSON, "");
                return;
            }

            Method controllerMethod = controllerMethodUtil.getControllerMethodByPath(httpOperation, requestPath);

            List<String> requestParameter = new ArrayList<>();
            if (requestPath.contains("?")) {
                String[] requestPathTokens = requestPath.split("\\?");
                requestPath = requestPathTokens[0];
                String queryParameterValues = requestPathTokens[1];

                requestParameter = controllerMethodUtil.extractQueryParameterFromRequestPath(queryParameterValues);
            }

            Map<String, Object> pathParameter = controllerMethodUtil.extractPathParameterFromRequestPath(httpOperation, requestPath);
            Object[] parameterToInvokeControllerMethod = controllerMethodUtil.getParameterToInvokeControllerMethod(controllerMethod, requestParameter, pathParameter, requestBody);

            Class<?> controllerClass = controllerMethod.getDeclaringClass();
            Object aService = applicationContext.getBean(controllerClass.getCanonicalName(), controllerClass, true);


            Object object = controllerMethod.invoke(aService, parameterToInvokeControllerMethod);


            String message;
            if (object == null) {
                message = "OK";
            } else {
                message = RequestUtil.toJson(object, object.getClass());
            }

            RequestUtil.creatResponse(socketOutputStream, OK_STATUS_CODE, "OK", MIME_TYPE_JSON, message);

        } catch (InvocationTargetException e) {
            Class<? extends Throwable> targetExceptionClass = e.getTargetException().getClass();
            String targetExceptionMessage = e.getTargetException().getMessage();
            if (targetExceptionClass.equals(AlreadyExistsException.class)) {
                e.printStackTrace();
                logger.error("AlreadyExistException", e);
                RequestUtil.creatResponse(socketOutputStream, BAD_REQUEST_STATUS_CODE, "User Already Exists", MIME_TYPE_TEXT, targetExceptionMessage);
            } else if (targetExceptionClass.equals(NotAllowedException.class)) {
                logger.error("NotAllowedException", e);
                RequestUtil.creatResponse(socketOutputStream, NOT_ALLOWED_STATUS_CODE, "Not Allowed", MIME_TYPE_TEXT, targetExceptionMessage);
            } else if (targetExceptionClass.equals(RequestException.class)) {
                logger.error("Bad Request", e);
                RequestUtil.creatResponse(socketOutputStream, BAD_REQUEST_STATUS_CODE, "Bad Request", MIME_TYPE_TEXT, targetExceptionMessage);
            } else if (targetExceptionClass.equals(NotAuthorizedException.class)) {
                logger.error("NotAuthorizedException", e);
                RequestUtil.creatResponse(socketOutputStream, FORBIDDEN_STATUS_CODE, "Forbidden", MIME_TYPE_TEXT, targetExceptionMessage);
            } else if (targetExceptionClass.equals(RuntimeException.class)) {
                logger.error("Runtime Error", e);
                RequestUtil.creatResponse(socketOutputStream, INTERNAL_SERVER_ERROR_STATUS_CODE, "Internal Server Error", MIME_TYPE_TEXT, targetExceptionMessage);
            } else if (targetExceptionClass.equals(Exception.class)) {
                logger.error("Internal Server Error", e);
                RequestUtil.creatResponse(socketOutputStream, INTERNAL_SERVER_ERROR_STATUS_CODE, "Internal Server Error", MIME_TYPE_TEXT, targetExceptionMessage);
            } else if (targetExceptionClass.equals(ResourceNotFoundException.class)) {
                logger.error("ResourceNotFoundException", e);
                RequestUtil.creatResponse(socketOutputStream, RESOURCE_NOT_FOUND_STATUS_CODE, "Resource Not Found", MIME_TYPE_TEXT, targetExceptionMessage);
            }
        } catch (RequestException e) {
            logger.error("Bad Request", e);
            RequestUtil.creatResponse(socketOutputStream, BAD_REQUEST_STATUS_CODE, "Bad Request", MIME_TYPE_TEXT, e.getMessage());
        } catch (NotAuthorizedException e) {
            logger.error("NotAuthorizedException", e);
            RequestUtil.creatResponse(socketOutputStream, FORBIDDEN_STATUS_CODE, "Forbidden", MIME_TYPE_TEXT, e.getMessage());
        } catch (ResourceNotFoundException e) {
            logger.error("ResourceNotFoundException", e);
            RequestUtil.creatResponse(socketOutputStream, RESOURCE_NOT_FOUND_STATUS_CODE, "Resource Not Found", MIME_TYPE_TEXT, e.getMessage());
        } catch (NotAllowedException e) {
            logger.error("NotAllowedException", e);
            RequestUtil.creatResponse(socketOutputStream, NOT_ALLOWED_STATUS_CODE, "Not Allowed", MIME_TYPE_TEXT, e.getMessage());
        } catch (AlreadyExistsException e) {
            e.printStackTrace();
            logger.error("AlreadyExistException", e);
            RequestUtil.creatResponse(socketOutputStream, BAD_REQUEST_STATUS_CODE, "User Already Exists", MIME_TYPE_TEXT, e.getMessage());
        } catch (RuntimeException e) {
            logger.error("Runtime Error", e);
            RequestUtil.creatResponse(socketOutputStream, INTERNAL_SERVER_ERROR_STATUS_CODE, "Internal Server Error", MIME_TYPE_TEXT, e.getMessage());
        } catch (Exception e) {
            logger.error("Internal Server Error", e);
            RequestUtil.creatResponse(socketOutputStream, INTERNAL_SERVER_ERROR_STATUS_CODE, "Internal Server Error", MIME_TYPE_TEXT, e.getMessage());
        }
    }

    /**
     * Checks if authorisation needed to send a Request is given / Username and Passwort matches with the one from the Database
     *
     * @param authorization the Passwort
     */
    private void checkAuthorization(String authorization) {
        if (authorization == null || "".equalsIgnoreCase(authorization)) {
            throw new NotAuthorizedException("Not Authorized");
        }

        final String authorizationEnc = authorization.replace("Basic ", "");
        final String authorizationDecoded = new String(Base64.getDecoder().decode(authorizationEnc));
        final String[] data = authorizationDecoded.split(":");
        final String username = data[0];
        final String password = data[1];

        if (!username.equals(webserverAdmin) || !password.equals(webserverAdminPassword)) {
            throw new NotAuthorizedException("Wrong user / password");
        }
    }

    /**
     * @return a Map of all read lines of bufferedReader,
     * the Input Stream with Information of the Header from the Webserver Socket
     */
    private Map<String, String> getHeader(BufferedReader bufferedReader) throws IOException {
        Map<String, String> headerMap = new HashMap<>();
        String headerLine;

        while ((headerLine = bufferedReader.readLine()).length() != 0) {
            String[] data = headerLine.split(": ");
            if (data.length == 1) {
                String[] httpHeader = data[0].split(" ");
                if (httpHeader.length != 3) {
                    throw new RequestException("Wrong http header");
                }
                headerMap.put("HTTP_OPERATION", httpHeader[0]);
                headerMap.put("HTTP_PATH", httpHeader[1]);
                headerMap.put("HTTP_VERSION", httpHeader[2]);

            } else if (data.length == 2) {
                if (data[0].equals("Authorization")) {
                    String[] httpHeader = data[1].split(" ");
                    headerMap.put(AUTHORIZATION, httpHeader[1]);
                }
                headerMap.put(data[0].trim(), data[1].trim());

            } else {
                logger.warn("Unknown header found: " + headerLine);
            }
            logger.info(headerLine);
        }

        return headerMap;
    }

    /**
     * Invokes a controllerMethod,
     * to get matching Method müssen wir herausfinden, ob die passende Methode über die übergebende parameter schon registriert worden ist...
     * falls eine gefunden parameter aus request path...
     * Ergebnis der methode zurück
     * dann response mit Ergebnis an socket
     *
     * @param httpOperation
     * @param requestPath
     * @param requestBody
     * @param socketOutputStream
     */


}
