package com.qs.webserver.framework.service;

import com.qs.webserver.framework.annotation.Factory;
import com.qs.webserver.framework.annotation.Produces;
import com.qs.webserver.framework.annotation.PropertyValue;
import com.qs.webserver.framework.util.logger.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Factory
public class ConnectionFactory {

  public final Logger logger = Logger.getLogger(ConnectionFactory.class);

  @PropertyValue(name = "app.jdbc.url")
  private String jdbcUrl;

  @PropertyValue(name = "app.jdbc.user")
  private String user;

  @PropertyValue(name = "app.jdbc.password")
  private String password;

  @Produces
  public Connection createConnection() {
    try {
      return DriverManager.getConnection(jdbcUrl, user, password);
    } catch (SQLException e) {
      logger.error(String.format("PSQL Exception = message: %s , Error Code: %s", e.getMessage(), e.getSQLState()), e);
      throw new RuntimeException("Could not establish SQL connection", e);
    }
  }
}
