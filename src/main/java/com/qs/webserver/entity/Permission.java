package com.qs.webserver.entity;

import com.qs.webserver.framework.annotation.Column;
import com.qs.webserver.framework.annotation.Id;
import com.qs.webserver.framework.annotation.Table;

@Table(table_name = "ws_permission")
public class Permission {

  @Id
  @Column(column_name = "id")
  private Long id;

  @Column(column_name = "permission_name")
  private String name;
}
