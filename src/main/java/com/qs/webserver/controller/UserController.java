package com.qs.webserver.controller;

import com.qs.webserver.dto.UserDto;
import com.qs.webserver.entity.User;
import com.qs.webserver.framework.annotation.*;
import com.qs.webserver.framework.exception.ResourceNotFoundException;
import com.qs.webserver.service.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller(path = "/user")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * Save new {@link UserDto}
     *
     * @param userDto the given {@link UserDto} which should be saved
     * @return the saved {@link UserDto}
     */
    @PostOperation
    public UserDto saveUser(@RequestBody UserDto userDto) {
        User user = userService.saveUser(userDto);
        userDto = new UserDto(user);

        return userDto;
    }

    @GetOperation()
    public Set<User> getUsers(@QueryParameter(name = "email") List<String> email) {
        Set<User> userSet = new HashSet<>();
        if (email != null && !email.isEmpty()) {
            email.forEach(aEmail -> {
                User userByEmail = userService.getUserByEmail(aEmail);
                userSet.add(userByEmail);
            });

            return userSet;
        }

        return userService.getAllUsers();
    }

    /*
     * Return a {@link UserDto} matching the given userId
     *
     * @param userId the given id of the new {@link UserDto} which should be returned
     * @return {@link User} found by given userId
     */
    @GetOperation(path = "/{userId}")
    public UserDto getUserById(@PathParameter(name = "userId") Long userId) {
        User user = userService.getUserById(userId);

        return new UserDto(user);
    }

    /**
     * Updates the given {@link UserDto}
     *
     * @param userDto the {@link UserDto } which should be Updated
     * @return the updated {@link UserDto}
     */
    @PutOperation(path = "/{userId}")
    public UserDto updateUser(@PathParameter(name = "userId") Long userId, @RequestBody UserDto userDto) {

        User user = userService.getUserById(userId);
        if (user == null) {
            throw new ResourceNotFoundException("User Not found. User id=" + userId);
        }

        userDto.setId(userId);
        user = userService.updateUser(userDto);

        return new UserDto(user);
    }

    /**
     * Delete {@link User} matching the userId
     *
     * @param userId the given userId of the user which should be deleted
     */
    @DeleteOperation(path = "/{userId}")
    public void deleteUser(@PathParameter(name = "userId") Long userId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new ResourceNotFoundException("User Not found. User id=" + userId);
        }
        userService.deleteUser(user);
    }
}
