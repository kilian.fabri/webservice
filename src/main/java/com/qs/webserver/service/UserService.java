package com.qs.webserver.service;

import com.qs.webserver.dto.UserDto;
import com.qs.webserver.entity.User;
import com.qs.webserver.framework.annotation.Autowired;
import com.qs.webserver.framework.annotation.Service;
import com.qs.webserver.repository.UserRepository;

import java.util.Set;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    /**
     * Create new {@link User}
     *
     * @param user the given {@link UserDto} which should be added. Never null.
     * @return the newly created {@link User}
     */
    public User saveUser(UserDto user) {
        User savedUser = new User();
        savedUser.setId(user.getId());
        savedUser.setName(user.getName());
        savedUser.setUsername(user.getUsername());
        savedUser.setEmail(user.getEmail());
        savedUser.setPassword(user.getPassword());

        return userRepository.createUser(savedUser);
    }

    public Set<User> getAllUsers() {

        return userRepository.findAllUsers();
    }

    /**
     * Return a {@link User} matching the given userId
     *
     * @param userId the given id of the user which should be returned
     * @return {@link User} found by given userId
     */
    public User getUserById(Long userId) {

        return userRepository.findUserById(userId);
    }

    public User getUserByEmail(String email) {

        return userRepository.findUserByEmail(email);
    }

    /**
     * Updates the given {@link UserDto}
     *
     * @param userDto the {@link UserDto} which should be updated
     * @return the newly updated {@link User}
     */
    public User updateUser(UserDto userDto) {
        User savedUser = new User();
        savedUser.setId(userDto.getId());
        savedUser.setName(userDto.getName());
        savedUser.setUsername(userDto.getUsername());
        savedUser.setEmail(userDto.getEmail());
        savedUser.setPassword(userDto.getPassword());

        return userRepository.updateUser(savedUser);
    }

    /**
     * Delete given {@link User}
     *
     * @param user the given {@link User} which should be deleted. Never null.
     */
    public void deleteUser(User user) {
        userRepository.deleteUser(user);
    }
}
