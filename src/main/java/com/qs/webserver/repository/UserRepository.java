package com.qs.webserver.repository;

import com.qs.webserver.entity.User;
import com.qs.webserver.framework.annotation.Autowired;
import com.qs.webserver.framework.annotation.Service;
import com.qs.webserver.framework.exception.AlreadyExistsException;
import com.qs.webserver.framework.jdbc.GenerateSQLStatements;
import com.qs.webserver.framework.util.logger.Logger;

import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.qs.webserver.framework.util.sqlStatements.SqlStatements.SQL_UPDATE_USER;
import static com.qs.webserver.framework.util.sqlStatements.SqlStatements.TEST_SQL_CREATE_TABLE;

@Service
public class UserRepository {

    public final Logger logger = Logger.getLogger(UserRepository.class);
    @Autowired
    public GenerateSQLStatements generateSQLStatements;
    @Autowired
    private Connection connection;

    public void createTable() {
        try (PreparedStatement st = connection.prepareStatement(TEST_SQL_CREATE_TABLE)) {
            //try (PreparedStatement st = connection.prepareStatement(generateSQLStatements.getCreateTableStatement())) {
            st.executeUpdate();
            logger.info("New Table Created");
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s, Error Code: %s", e.getMessage(), e.getSQLState());
            throw new RuntimeException("Exception while creating Table");
        }
    }

    public Set<User> findAllUsers() {
        Set<User> userSet = new LinkedHashSet<>();

        try (PreparedStatement st = connection.prepareStatement(generateSQLStatements.getFindAllSQLStatement());
             ResultSet resultSet = st.executeQuery()) {
            while (resultSet.next()) {
                Long userId = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String userEmail = resultSet.getString("email");
                String username = resultSet.getString("username");
                String userPassword = resultSet.getString("password");
                userSet.add(new User(userId, name, userEmail, username, userPassword));
            }

            return userSet;
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s, Error Code: %s", e.getMessage(), e.getSQLState());
            throw new RuntimeException("Exception while finding User");
        }
    }

    public int countAllUsers() {
        try (PreparedStatement st = connection.prepareStatement(generateSQLStatements.getCountStatement());
             ResultSet resultSet = st.executeQuery()) {
            if (resultSet.next()) {
                final int count = resultSet.getInt("count");
                System.out.println(count);
                return count;
            }
            return 0;
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s, Error Code: %s", e.getMessage(), e.getSQLState());
            throw new RuntimeException("Exception while counting all Users");
        }
    }

    /**
     * Create a new {@link User} in database
     *
     * @param user the new {@link User} which should be put into the database
     * @return the newly created {@link User}
     */
    public User createUser(User user) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(generateSQLStatements.getSaveUserSQlStatement(), Statement.RETURN_GENERATED_KEYS)) {
            connection.setAutoCommit(false);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getUsername());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                long userId = 0;
                if (generatedKeys.next()) {
                    userId = generatedKeys.getLong(1);
                }
                connection.commit();
                user.setId(userId);

                return user;
            }
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s , Error Code: %s", e.getMessage(), e.getSQLState(), e);
            throw new AlreadyExistsException("Exception while creating user", e);
            //throw new RuntimeException("Exception while creating user", e);
        } /*finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }*/
    }

    /**
     * Find {@link User} matching the given id in database
     *
     * @param id the id with which the {@link User} should be found
     * @return the found {@link User}
     */
    public User findUserById(Long id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(generateSQLStatements.getFindByIdSQLStatement())) {
            preparedStatement.setLong(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return getUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s , Error Code: %s", e.getMessage(), e.getSQLState(), e);
            throw new RuntimeException("Exception while finding user", e);
        }
    }

    public User findUserByEmail(String email) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(generateSQLStatements.getFindByEmailSQLStatement())) {
            preparedStatement.setString(1, email);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return getUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s , Error Code: %s", e.getMessage(), e.getSQLState(), e);
            throw new RuntimeException("Exception while finding user", e);
        }
    }

    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Long userId = resultSet.getLong("id");
            String name = resultSet.getString("name");
            String userEmail = resultSet.getString("email");
            String username = resultSet.getString("username");
            String userPassword = resultSet.getString("password");
            return new User(userId, name, userEmail, username, userPassword);
        }
        return null;
    }

    /**
     * Updates the given {@link User} in Database
     *
     * @param user the {@link User} which should be updated
     * @return the updated {@link User}
     */
    public User updateUser(User user) {
        Long id = user.getId();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getUsername());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setLong(5, id);
            preparedStatement.executeUpdate();

            return user;
        } catch (SQLException e) {
            if (e.getSQLState().equalsIgnoreCase("23505")) {
                throw new AlreadyExistsException("User already exists", e);
            }
            logger.error("PSQL Exception = message: %s , Error Code: %s", e.getMessage(), e.getSQLState(), e);
            throw new RuntimeException("Exception while updating user", e);
        }
    }

    /**
     * Delete given {@link User} in Database
     *
     * @param user the given {@link User} which should be deleted. Never null.
     */
    public void deleteUser(User user) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(generateSQLStatements.getDeleteByIdSQLStatement())) {
            preparedStatement.setLong(1, user.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            logger.error("PSQL Exception = message: %s , Error Code: %s", e.getMessage(), e.getSQLState(), e);
            throw new RuntimeException("Exception while deleting user", e);
        }
    }
}
