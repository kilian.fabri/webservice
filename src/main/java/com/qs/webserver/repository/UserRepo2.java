package com.qs.webserver.repository;

import com.qs.webserver.dto.UserDto;
import com.qs.webserver.entity.User;
import com.qs.webserver.framework.annotation.Repository;
import com.qs.webserver.framework.repository.CrudRepository;

import java.util.Optional;

@Repository
public interface UserRepo2 extends CrudRepository<User, Long> {
  Optional<User> findByEmail(String email);
}
